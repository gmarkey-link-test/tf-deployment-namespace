variable "target_eks" {

  description = "Name of the target EKS cluster."
  type        = string
  default     = "default_demo_app"

}


variable "ci_runner" {

  description = "Enable or disable creation of CI runner"
  type        = number
  default     = 1

}


variable "namespace" {

  description = "Name of the namespace we are furnishing."
  type        = string

}


variable "ci_runner_registration_token" {

  description = "Gitlab CI runner registration token."
  type        = string
  default     = "hNytw-mRf7rAgYVsTnmR"

}


locals {

  workspace = replace(terraform.workspace, "_", "-")

}
