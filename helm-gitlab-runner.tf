locals {

  gitlab_runner_values = <<-EOS

    # For full list of options, see here:
    # https://docs.gitlab.com/charts/
    gitlabUrl: https://gitlab.com
    runnerRegistrationToken: "${var.ci_runner_registration_token}"
    fullnameOverride: gitlab-runner

    rbac:
      create: true
      podSecurityPolicy:
        enabled: true

    runners:
      runUntagged: false
      serviceAccountName: gitlab-runner-executor

    EOS

}


resource "helm_release" "gitlab_runner" {

  depends_on       = [ kubernetes_namespace.furnished ]
  count            = var.ci_runner
  name             = "gitlab-runner"
  chart            = "gitlab-runner"
  repository       = "https://charts.gitlab.io"
  namespace        = var.namespace
  create_namespace = false
  values           = [ local.gitlab_runner_values ]
  wait             = false

  set {
    name  = "runners.tags"
    value = var.namespace
    type  = "string"
  }

}
