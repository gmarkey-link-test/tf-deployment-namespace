resource "kubernetes_namespace" "furnished" {

  metadata {
    name   = var.namespace
    labels = {
      name = var.namespace
    }
  }

}


resource "kubernetes_service_account" "gitlab_runner_executor" {

  count                           = var.ci_runner
  depends_on                      = [ kubernetes_namespace.furnished ]
  automount_service_account_token = true

  metadata {
    name      = "gitlab-runner-executor"
    namespace = var.namespace
  }

}


resource "kubernetes_role" "gitlab_runner_executor" {

  count      = var.ci_runner
  depends_on = [ kubernetes_namespace.furnished ]

  metadata {
    name      = "gitlab-runner-executor"
    namespace = var.namespace
  }

  rule {
    api_groups = ["", "batch", "apps", "networking.k8s.io", "storage.k8s.io", "rbac.authorization.k8s.io", "cert-manager.io"]
    resources  = ["*"]
    verbs      = ["*"]
  }

}


resource "kubernetes_role_binding" "gitlab_runner_executor" {

  count      = var.ci_runner
  depends_on = [ kubernetes_role.gitlab_runner_executor, kubernetes_service_account.gitlab_runner_executor ]

  metadata {
    name      = "gitlab-runner-executor"
    namespace = var.namespace
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = "gitlab-runner-executor"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "gitlab-runner-executor"
    namespace = var.namespace
  }

}
